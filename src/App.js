import React,{Component} from 'react';
// import logo from './logo.svg';
import {Route,Switch,BrowserRouter} from  'react-router-dom'
import './App.css';
import BarMenu from './Component/Dasbord/barMenu/barMenu'

import NavBar from './Component/Dasbord/navbar/navbar'
import { Row,Col } from 'react-bootstrap';
import MenuDrink from '../src/Component/Dasbord/drink/drink'
import MenuDrinkAdd from '../src/Component/Dasbord/drink/addDrink'
import MenuDrinkEdit from '../src/Component/Dasbord/drink/editDrink'
import MenuFood from '../src/Component/Dasbord/food/food'
import MenuFoodAdd from '../src/Component/Dasbord/food/addFood'
import MenuEmployee from '../src/Component/Dasbord/employee/employee'
import MenuFoodEdit from '../src/Component/Dasbord/food/editFood'
import MenuEmployeeAdd from '../src/Component/Dasbord/employee/addEmployee'
import MenuEmployeeEdit from '../src/Component/Dasbord/employee/editEmployee'
import Show from '../src/Component/Dasbord/navbar/show'
import Error from '../src/Component/Dasbord/error'
import { EROFS } from 'constants';


function employee(props){
  return <MenuEmployee props={props}/>
}
function employeeEdit(props){
  return <MenuEmployeeEdit props={props}/>
}
function employeetmbh(props){
  return <MenuEmployeeAdd props={props}/>
}
function drinkEdit(props){
  return <MenuDrinkEdit props={props}/>
}
function drinkAdd(props){
  return <MenuDrinkAdd props={props}/>
}
function foodEdit(props){
  return <MenuFoodEdit props={props}/>
}
function makanantmbh(){
  return <MenuFoodAdd/>
}
function Home(){
  return <Show/>
}
function food(props){
  return <MenuFood props={props}/>
}
function drink(props){
  return <MenuDrink props={props}/>
}
function add(){
  return<h1>Add</h1>
}
function noMatch(){
  return  <Error/>
}
class App extends Component{
  render(){
    return(
      <div className="dasar">
        <div className="box"></div>
        <BrowserRouter>
        
      <div>
              <BarMenu/>
              <NavBar/>
      </div>
      <div> 
      <div className="isi">
      <main className="ganti">
                    <Switch>
                            <Route path='/' exact component={Home}/>

                            <Route path='/employee/edit/:id/' exact component={employeeEdit}/>
                            <Route path='/employee/tambah/' exact component={employeetmbh}/>
                            <Route path='/employee/' exact component={employee}/>
                            <Route path='/employee/tambah' exact component={employeetmbh}/>
                            <Route path='/employee' exact component={employee}/>
                            <Route path='/employee/delete/:id/' exact component={employee}/>

                            <Route path='/food/edit/:id/' exact component={foodEdit}/>
                            <Route path='/food/tambah/' exact component={makanantmbh}/>
                            <Route path='/food' exact component={food}/>
                            <Route path='/food/tambah' exact component={makanantmbh}/>
                            <Route path='/food' exact component={food}/>
                            <Route path='/food/delete/:id/' exact component={food}/>
                            
                            <Route path='/drink/' exact component={drink}/>
                            <Route path='/drink/tambah/' exact component={drinkAdd}/>
                            <Route path='/drink' exact component={drink}/>
                            <Route path='/drink/tambah' exact component={drinkAdd}/>
                            <Route path='/drink/edit/:id/' exact component={drinkEdit}/>
                            <Route path='/add/' exact component={add}/>
                            <Route component={noMatch}/>
                    </Switch>
                    </main>
      
                    </div>
     
     
      </div>
      </BrowserRouter>
      </div>
    )
  }
}

export default App;
